#ifndef LEFTIST_H
#define LEFTIST_H
#include<iostream>
#include<algorithm>
#include<vector>
#include<map>

using namespace std;

template <class T> class HeapNode{
    public:
        HeapNode *lefttson;
        HeapNode *rightson;
        HeapNode *father;
        int dist;
        T value;
};

template <class T> class Leftist{
    public:
        Leftist(int MaxSize = 100000){
            root = NULL;
            Map.resize(MaxSize,NULL);
        }

        bool empty(){
            return root == NULL;
        }

        void push(T x){
            root = mergeHeap(root,newHeap(x));
            if(root) root->father = NULL;
        }

        T top(){
            return root->value;
        }

        void pop(){
            HeapNode<T> *L = root->lefttson;
            HeapNode<T> *R = root->rightson;
            Map.erase(root->value);
            delete root;
            root = mergeHeap(L,R);
            if(root) root->father = NULL;
        }

        void decrease_key(T x, T y){
            if(Map[x.id] == NULL) return ;
            deleteNode(x);
            push(y);
        }

    private:
        HeapNode<T> *root;
        vector<HeapNode<T> *> Map;

        HeapNode<T> *newHeap(T x) {
           HeapNode<T> *newNode = new HeapNode<T>;
            newNode->father = newNode->lefttson = newNode->rightson = NULL;
            newNode->value  = x;
            newNode->dist   = 0;
            Map[x.id]  = newNode;
            return newNode;
        }

        HeapNode<T> *mergeHeap(HeapNode<T> *A, HeapNode<T> *B){
            if(! A) return B;
            if(! B) return A;
            if(B->value < A->value) swap(A,B);
            A->rightson = mergeHeap(A->rightson,B);
            if((! A->lefttson) || (A->rightson->dist > A->lefttson->dist)){
                swap(A->lefttson,A->rightson);
            }
            A->dist = (A->rightson!=NULL)?(1+A->rightson->dist):0;
            if(A->rightson) A->rightson->father = A;
            if(A->lefttson) A->lefttson->father = A;
            return A;
        }

        void Pushup(HeapNode<T> *Now){
            if(! Now) return ;
            if((! Now->lefttson) || (Now->rightson && Now->lefttson->dist < Now->rightson->dist)){
                swap(Now->lefttson, Now->rightson);
            }
            if(! Now->rightson){
                if(Now->dist == 0) return ;
                Now->dist = 0;
                Pushup(Now->father);
            }
            else if(Now->dist != 1+Now->rightson->dist){
                Now->dist = 1+Now->rightson->dist;
                Pushup(Now->father);
            }
        }

        void deleteNode(T x){
            HeapNode<T> *Now = Map[x.id]; 
            if(Now == root) {
                pop();
                return;
            }
            Map.erase(x);
            HeapNode<T> *fa = Now->father;
            if(fa->lefttson == Now){
                fa->lefttson = mergeHeap(Now->lefttson, Now->rightson);
                if(fa->lefttson) fa->lefttson->father = fa;
            }else {
                fa->rightson = mergeHeap(Now->lefttson, Now->rightson);
                if(fa->rightson) fa->rightson->father = fa;
            }  
            delete Now;
            Pushup(fa);
        }

};

#endif