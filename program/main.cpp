#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#pragma GCC optimize("O4")
#pragma GCC optimize("O5")
#pragma GCC optimize("O6")
#pragma GCC optimize("O2")
#pragma GCC optimize("O7")
#pragma GCC optimize("O8")
#pragma GCC optimize("O9")
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("-fipa-sra")
#pragma GCC optimize("-ftree-pre")
#pragma GCC optimize("-ftree-vrp")
#pragma GCC optimize("-fpeephole2")
#pragma GCC optimize("-ffast-math")
#pragma GCC optimize("-fsched-spec")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("-falign-jumps")
#pragma GCC optimize("-falign-loops")
#pragma GCC optimize("-falign-labels")
#pragma GCC optimize("-fdevirtualize")
#pragma GCC optimize("-fcaller-saves")
#pragma GCC optimize("-fcrossjumping")
#pragma GCC optimize("-fthread-jumps")
#pragma GCC optimize("-funroll-loops")
#pragma GCC optimize("-fwhole-program")
#pragma GCC optimize("-freorder-blocks")
#pragma GCC optimize("-fschedule-insns")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("-ftree-tail-merge")
#pragma GCC optimize("-fschedule-insns2")
#pragma GCC optimize("-fstrict-aliasing")
#pragma GCC optimize("-fstrict-overflow")
#pragma GCC optimize("-falign-functions")
#pragma GCC optimize("-fcse-skip-blocks")
#pragma GCC optimize("-fcse-follow-jumps")
#pragma GCC optimize("-fsched-interblock")
#pragma GCC optimize("-fpartial-inlining")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("-freorder-functions")
#pragma GCC optimize("-findirect-inlining")
#pragma GCC optimize("-fhoist-adjacent-loads")
#pragma GCC optimize("-frerun-cse-after-loop")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("-ftree-switch-conversion")
#pragma GCC optimize("-foptimize-sibling-calls")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-funsafe-loop-optimizations")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fdelete-null-pointer-checks")
#include <ctime>
#include <stdlib.h>
#include <cstdio>
#include <ctime>
#include "fibonacci.h"
#include "binary.h"
#include "leftist.h"

#define INF 0x7fffffff
#define MaxN 10000010
using namespace std;
 
struct Edge{ // store edges
    int To,Dis,Next;
}List[MaxN*4];

struct Node{ 
    int id,dis; 
    bool operator<(const Node &c) const{
        return dis < c.dis;
    }
    bool operator>(const Node &c) const{
        return dis > c.dis;
    }
};

struct Node1
{
	int x,y;
}Q2[1010];


bool cmp(Node1 a,Node1 b)
{
	return a.x<b.x;
}

int N,M,K,Q1,op;
int S1;
int head[MaxN];
void addEdge(int u,int v,int w){ //Building graph by adjacency table
    ++K;
    //cout<<K<<endl;
    List[K].To  = v;
    List[K].Dis = w;
    List[K].Next = head[u];
    head[u] = K;
}

int Distance[MaxN],Visit[MaxN],flag[MaxN];
void Dijkstra(int S,auto Q){ //model of dijkstra algorithm for priority queue. S: start point Q: the queue
    for(int i = 1;i <= N; ++i) Distance[i] = 1e9+7,flag[i]=0,Visit[i]=0;//Assigning Initial Values to a Distance Array
	Distance[S] = 0,flag[S] = 1;
	Q.push((Node){S,0}); 

    while(!Q.empty())
    {
        Node now = Q.top();
        int pt = now.id;
        Q.pop();
        if(Visit[pt]) continue;
        Visit[pt] = 1;
        if(pt == Q2[op].y) return;
        for(int j = head[pt];j != 0; j = List[j].Next)
        {
           int to = List[j].To, dis = List[j].Dis;
           if( !Visit[to] && (dis+Distance[pt]) < Distance[to] ) 
           {
           	 if(flag[to])
              Q.decrease_key((Node){to,Distance[to]},(Node){to,dis+Distance[pt]});
              else
               Q.push((Node){to,dis+Distance[pt]}),flag[to]=1;
			 Distance[to] = dis+Distance[pt];
           }  
        }
    }
}


void runFibonacci(){ // testing runtime

    static Fibonacci<Node> q(N+1);
    Dijkstra(S1,q);
}

void runBinary(){ // testing runtime

    static Binary<Node> q;
    Dijkstra(S1,q);
}

void runLeftist(){ // testing runtime
    static Binary<Node> q;
    Dijkstra(S1,q);
}



int main()
{
	freopen("randata/O(n^2)/5.in","r",stdin);
	freopen("randata/O(n^2)/5.out","w",stdout);
    cin >> N >> M;
    int u,v,w;
    for(int i = 1;i <= M; i++){
        cin >> u >> v >> w;
        addEdge(u,v,w);
        //addEdge(v,u,w);
    }
    scanf("%d",&Q1);
    for(int i = 1;i <= Q1; i++)
	   scanf("%d%d",&Q2[i].x,&Q2[i].y); 
    clock_t sttime,entime,sttime1,entime1,sttime2,entime2;
    sttime = clock();
	for(int i = 1;i <= Q1; i++)
    {
	 S1=Q2[i].x,op=i,runFibonacci();
     printf("%d ", Distance[Q2[i].y]);
    }
    printf("\n");
    entime = clock();
    sttime1 = clock();
	for(int i = 1;i <= Q1; i++)
    {
	 S1=Q2[i].x,op=i,runBinary();
     printf("%d ", Distance[Q2[i].y]);
    }
    printf("\n");
    entime1 = clock();
    sttime2 = clock();
	for(int i = 1;i <= Q1; i++)
    {
	 S1=Q2[i].x,op=i,runLeftist();
     printf("%d ", Distance[Q2[i].y]);
    }
    printf("\n");
    entime2 = clock();
    cout<<"Fibonacci heaps run time: "<<(double)(entime - sttime) / CLOCKS_PER_SEC<<"s"<<endl;
    cout<<"Binary heaps run time: "<<(double)(entime1 - sttime1) / CLOCKS_PER_SEC<<"s"<<endl;
    cout<<"Leftist heaps run time: "<<(double)(entime2 - sttime2) / CLOCKS_PER_SEC<<"s"<<endl;
    return 0;
}
