#ifndef BINARY_H
#define BINARY_H
#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;

template <class T> class Binary{
    public:
    Binary() {
    	Tree.clear();
    	Tree.resize(1);
    	Id.clear();
    	Id.resize(10);
    	tot2 = tot = 0;
    }
    bool empty() {
    	return tot == 0;
    }

    void push(T x) //insert x
    {
    	if(tot == Tree.size()-1) {
    		Tree.resize(Tree.size()*2);
        }
        if(x.id>=Id.size())
        {
            Id.resize(x.id+1);
        }
        int tot1 =++tot;
        for(;tot1>1&&x<Tree[tot1/2];tot1>>=1)
        {
         Id[Tree[tot1/2].id]=tot1;
         Tree[tot1]=Tree[tot1/2];
        }
        Id[x.id]=tot1;Tree[tot1]=x;
    }

    T top()
    {
        return Tree[1];
    }

    void pop()
    {
    	if(empty()) {
    		return;
        }
    	Id[Tree[tot].id] = 1;Tree[1] = Tree[tot];--tot;
    	int now=1;
    	while(now*2<=tot)
		 {
		  int child;
		  child=now<<1; 
    	  if(child!=tot&&Tree[child]>Tree[child+1])
    	    child++;
    	  if(Tree[now]>Tree[child])
    	  {
    	  	int t,x,y;
    	  	T t1;
    	  	x=Tree[now].id;y=Tree[child].id;
            t=Id[x];Id[x]=Id[y];Id[y]=t;
    	  	t1=Tree[child];Tree[child]=Tree[now];Tree[now]=t1;
		    }
          else break;
          now=child;
      }
    }

    void decrease_key(T x,T y)
    {
    	int now=Id[x.id];
    	Tree[now]=y;
    	for(;now>1&&Tree[now]<Tree[now/2];now/=2)
    	{
          int t,x,y;
          T t1;
          x=Tree[now].id;y=Tree[now/2].id;
          t=Id[x];Id[x]=Id[y];Id[y]=t;
          t1=Tree[now];Tree[now]=Tree[now/2];Tree[now/2]=t1;
        }
    }

	void print()
	{
	    int i;
	    cout<<tot<<endl;
		for(i=1;i<=tot;++i)
		  cout<<Tree[i].id<<" "<<Tree[i].dis<<endl;
        cout<<endl<<endl<<endl;
    }

    private:
      vector <T> Tree;
      vector <int> Id;
      int tot,tot2;
};

// Implementation for Binary Heap
#endif
