#ifndef FIBONACCI_H
#define FIBONACCI_H
#include<iostream>
#include<algorithm>
#include<map>
#include<vector>

using namespace std;
template <class T> class TreeNode{
    public:
        T value;
        TreeNode *father;
        TreeNode *son;
        TreeNode *sib;
        bool isMarked;
        int rank;
};
     

template <class T> class Tree{
    public:
        TreeNode<T> *root;
        Tree *Next;
        Tree *Last;

};


template <class T> class Fibonacci{
    public:
        Fibonacci(int MaxSize = 10000){
            MinTree = NULL;
            Map.resize(MaxSize,NULL);
        }

        bool empty(){
            return MinTree == NULL;
        }
        
        void push(T x){ //Insert operation
            Tree<T> *NewTree = new Tree<T>;
            TreeNode<T> *NewNode = new TreeNode<T>;
            NewNode->value = x;
            NewNode->sib = NewNode->son = NewNode->father = NULL;
            NewNode->isMarked = false;
            NewTree->root = NewNode;
            NewTree->Last = NewTree->Next = NewTree;
            NewNode->rank = 0;
            Map[x.id] = NewNode;
            insertTree(NewTree);
            if(NewTree->root->value < MinTree->root->value) MinTree = NewTree;
        } 

        T top(){
            return MinTree->root->value;
        }

        void pop(){ //delete min
            TreeNode<T> *tmp;
            for(TreeNode<T> *v = MinTree->root->son; v != NULL; v = tmp){
                tmp = v->sib;
                insertRootLink(v);
            }

            Earse();

            Upgrade();
        }
        
        void decrease_key(T x,T y) {// change x -> y and x > y and x.id == y.id
            if(Map[x.id] == NULL) return ;
            TreeNode<T> *v = Map[x.id];
            v->value = y;
            if(v->father != NULL && v->father->value > y){
                v->isMarked = true;
                cutUp(v);
            }
            updateMinTree();
        }

    private:
        Tree<T> *MinTree;
        vector<TreeNode<T> *> Map;
        void insertTree(Tree<T> *tmp){ 
            if(! MinTree) MinTree = tmp;
            else {
                tmp->Next = MinTree->Next;
                MinTree->Next->Last = tmp;
                MinTree->Next = tmp;
                tmp->Last = MinTree;
            }            
        }

        void insertRootLink(TreeNode<T> *v){
            (v->father->rank)--;
            if(v->father->son == v) v->father->son = v->sib;
            else {
                TreeNode<T> *las;
                for(las = v->father->son; las->sib != v && las != NULL; las = las->sib) ;
                las->sib = v->sib;
            }
            v->father = v->sib = NULL;
            Tree<T> *NewTree = new Tree<T>;
            NewTree->root = v;
            NewTree->Last = NewTree->Next = NewTree;
            insertTree(NewTree);            
        }

        void deletePos(Tree<T> *Now){        
            Now->Last->Next = Now->Next;
            Now->Next->Last = Now->Last;
        }

        void updateMinTree(){
            Tree<T> *tmp = MinTree;
            for(Tree<T> *i = MinTree->Next; i != tmp; i = i->Next){ //update MinTree
                if(i->root->value < MinTree->root->value) MinTree = i;
            }            
        }

        void Earse(){ // drop MinTree
            Map[(MinTree->root->value).id] = NULL;
            if(MinTree->Next == MinTree){
                delete MinTree;
                MinTree = NULL; // empty heap
                return ;
            }
            Tree<T> *tmp = MinTree->Next;
            deletePos(MinTree);
            delete MinTree;
            MinTree = tmp;
            updateMinTree();
        }

        Tree<T> *mergeTree(Tree<T> *Pre, Tree<T> *Now) {
            deletePos(Pre);
            if(Pre->root->value > Now->root->value) {
                Pre->root->father = Now->root;
                Pre->root->sib = Now->root->son;
                Now->root->son = Pre->root;
                (Now->root->rank)++;

                delete Pre;
                return Now;                
            }
            else if(Pre->root->value < Now->root->value){
                Now->root->father = Pre->root;
                Now->root->sib = Pre->root->son;
                Pre->root->son = Now->root;
                (Pre->root->rank)++;

                Pre->Last = Now->Last;
                Now->Last->Next = Pre;
                Pre->Next = Now->Next;
                Now->Next->Last = Pre;

                delete Now;
                return Pre;
            }

            if(Pre == MinTree) {
                Now->root->father = Pre->root;
                Now->root->sib = Pre->root->son;
                Pre->root->son = Now->root;
                (Pre->root->rank)++;

                Pre->Last = Now->Last;
                Now->Last->Next = Pre;
                Pre->Next = Now->Next;
                Now->Next->Last = Pre;

                delete Now;
                return Pre;               
            }else {
                Pre->root->father = Now->root;
                Pre->root->sib = Now->root->son;
                Now->root->son = Pre->root;
                (Now->root->rank)++;

                delete Pre;
                return Now;                 
            }
        }

        void Upgrade(){ // keep the distinct rank
            static map<int,Tree<T> *> p;
            if(! MinTree) return ;
            p.clear();
            p[MinTree->root->rank] = MinTree;
            int D = 0;
            for(Tree<T> *Now = MinTree->Next;Now != MinTree; Now = Now->Next){
                D = Now->root->rank; 
                if(p.find(D) == p.end()) p[D] = Now;             
                else{ 
                    if(p[D] == Now) continue;
                    while(p.find(D) != p.end()) {
                        Now = mergeTree(p[D],Now);
                        p.erase(D);
                        D++;
                    }
                    p[D] = Now;
                }      
            }
        }

        void cutUp(TreeNode<T> *Now){
            if(Now->father == NULL) { // reached a root
                if(Now->isMarked == true) Now->isMarked = false;
                return ;
            }
            if(Now->isMarked == false) { // if unmark
                Now->isMarked = true;
                return ;
            }
            TreeNode<T> *fa = Now->father;
            insertRootLink(Now);
            Now->isMarked = false; // unmarked
            cutUp(fa);
        }
};


// Implementation for Fibonacci Heap
#endif
